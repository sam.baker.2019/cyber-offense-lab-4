# CVE: https://nvd.nist.gov/vuln/detail/CVE-2020-14144

# import base ubuntu image
FROM ubuntu

# update the image
RUN apt-get update -y

# install needed packages
RUN apt-get install git wget gpg systemd -y

# following the installation instructions found here: https://docs.gitea.io/en-us/installation/install-from-binary/
# download version 1.12.5 binary file and make it an executable
RUN wget -O gitea https://dl.gitea.com/gitea/1.12.5/gitea-1.12.5-linux-amd64 && \
    chmod +x gitea

# add a user called git that can run Gitea
RUN adduser \
   --system \
   --shell /bin/bash \
   --gecos 'Git Version Control' \
   --group \
   --disabled-password \
   --home /home/git \
   git

# create the required directory structure
#   adding the gitea directory to /var/lib and to /etc
#   adding /custom, /data, and /log directories to /var/lib/gitea
#   allow git user to access /var/lib/gitea
#   allow git user to access /etc/gitea
RUN mkdir -p /var/lib/gitea/{custom,data,log} && \
    chown -R git:git /var/lib/gitea/ && \
    chmod -R 750 /var/lib/gitea/ && \
    mkdir /etc/gitea && \
    chown root:git /etc/gitea && \
    chmod 770 /etc/gitea

# move the downloaded executable into the local bin and make sure it is still executable
RUN cp gitea /usr/local/bin/ && \
    chmod +x /usr/local/bin/gitea

# open port 3000, where gitea runs
EXPOSE 3000

# start the gitea service
CMD ["/usr/local/bin/gitea", "web", "--config", "/etc/gitea/app.ini"]
