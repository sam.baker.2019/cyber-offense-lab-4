# Cyber Offense Lab 4



The two CVE exploits that are attempted in this repo are [CVE-2020-14144 Gitea Code Execution](https://nvd.nist.gov/vuln/detail/CVE-2020-14144) and [CVE-2018-1335 Apache Tika Command Injection](https://nvd.nist.gov/vuln/detail/CVE-2018-1335).